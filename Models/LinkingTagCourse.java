package Models;

import javax.persistence.*;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * Created by insomniac on 16.05.15.
 */
@Entity
@Table(name = "linking_tag_course")
public class LinkingTagCourse implements Externalizable{

	@Id
	@OneToOne(targetEntity = Tags.class, fetch = FetchType.LAZY)
	@Column(name = "tag_id")
	public int tagId;

	@ManyToOne(targetEntity = Courses.class, fetch = FetchType.LAZY)
	@Column(name = "course_id")
	public int courseId;

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {

	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

	}
}
