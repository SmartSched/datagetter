package Models;

import javax.persistence.*;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * Created by insomniac on 15.05.15.
 */
@Entity
@Table(name = "students")
public class Students implements Externalizable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "_id", nullable = false)
	public int id;

	@Column(name = "first_name", length = 127)
	public String firstName;

	@Column(name = "last_name", length = 127)
	public String lastName;

	@Column(name = "e_mail", length = 127)
	public String eMail;

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {

	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

	}
}
