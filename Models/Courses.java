package Models;

import javax.persistence.*;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * Created by insomniac on 16.05.15.
 */
@Entity
@Table(name = "courses")
public class Courses implements Externalizable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "_id", nullable = false)
	public int id;

	@Column(name = "course_title", length = 255)
	public String courseTitle;

	@ManyToOne(targetEntity = Teachers.class, fetch = FetchType.LAZY)
	@Column(name = "teacher_id")
	public int teacherId;

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {

	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

	}
}
