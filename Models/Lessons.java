package Models;

import javax.persistence.*;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * Created by insomniac on 16.05.15.
 */
@Entity
@Table(name = "lessons")
public class Lessons implements Externalizable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "_id", nullable = false)
	public int id;

	@Column(name = "lesson_date")
	public String lessonDate;

	@Column(name = "lesson_num")
	public int lessonNum;

	@Column(name = "lesson_duration")
	public int lessonDuration;

	@ManyToOne(targetEntity = Places.class, fetch = FetchType.LAZY)
	@Column(name = "place_id")
	public int placeId;

	@ManyToOne(targetEntity = Teachers.class, fetch = FetchType.LAZY)
	@Column(name = "teacher_id")
	public int teacherId;

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {

	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

	}
}
