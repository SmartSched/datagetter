package Models;

import javax.persistence.*;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * Created by insomniac on 16.05.15.
 */
@Entity
@Table(name = "groups")
public class Groups implements Externalizable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "_id", nullable = false)
	public int id;

	@Column(name = "group_title", length = 64)
	public String groupTitle;

	@ManyToOne(targetEntity = Lessons.class, fetch = FetchType.LAZY)
	@Column(name = "lesson_id")
	public int lessonId;

	@ManyToOne(targetEntity = Groups.class, fetch = FetchType.LAZY)
	@Column(name = "group_id")
	public int groupId;

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {

	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

	}
}
