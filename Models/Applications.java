package Models;

import javax.persistence.*;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * Created by insomniac on 16.05.15.
 */
@Entity
@Table(name = "applications")
public class Applications implements Externalizable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "_id", nullable = false)
	public int id;

	@ManyToOne(targetEntity = Courses.class, fetch = FetchType.LAZY)
	@Column(name = "course_id")
	public int courseId;

	@ManyToOne(targetEntity = Groups.class, fetch = FetchType.LAZY)
	@Column(name = "group_id")
	public int groupId;

	@ManyToOne(targetEntity = Students.class, fetch = FetchType.LAZY)
	@Column(name = "student_id")
	public int studentId;

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

	}
}
