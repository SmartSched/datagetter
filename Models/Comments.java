package Models;

import javax.persistence.*;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * Created by insomniac on 16.05.15.
 */
@Entity
@Table(name = "comments")
public class Comments implements Externalizable {

	@Id
	@Column(name = "_id", nullable = false)
	public int id;

	@Column(name = "comment_text", length = 8192)
	public String commentText;

	@Column(name = "comment_rating")
	public int commentRating;

	@ManyToOne(targetEntity = Students.class, fetch = FetchType.LAZY)
	@Column(name = "student_id")
	public int studentId;

	@ManyToOne(targetEntity = Courses.class, fetch = FetchType.LAZY)
	@Column(name = "course_id")
	public int courseId;

	@ManyToOne(targetEntity = Courses.class, fetch = FetchType.LAZY)
	@Column(name = "lesson_id")
	public int LessonId;

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {

	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

	}
}
