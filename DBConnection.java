import com.mysql.fabric.jdbc.FabricMySQLDriver;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by insomniac on 15.05.15.
 */
public class DBConnection {

	public static final String URL = "jdbc:mysql://localhost:3306/labGUI4";
	public static final String USERNAME = "root";
	public static final String PASSWORD = "Speedhunter";

	private Connection connection;

	private static DBConnection instance = null;

	public static DBConnection getInstance(){
		if(instance == null){
			instance = new DBConnection();
		}
		return instance;
	}

	private DBConnection(){
		establishConnection();
	}

	private void establishConnection(){
		try{
			Driver driver = new FabricMySQLDriver();
			DriverManager.registerDriver(driver);

			connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);

			if(!connection.isClosed()){
				System.out.println("Connection with DB is configured!");
			}

		}catch (SQLException se){
			System.out.println("I don`t have a driver!");
		}
	}

	private void closeConnection(){
		try{
			connection.close();
		}catch (SQLException se){
			System.out.println("Problem with closing connection!");
		}
	}
}
